export default {
  "trailingSlash": true,
  "pathPrefix": "",
  "titleTemplate": "%s - DEV",
  "siteUrl": "https://delvvince.gitlab.io",
  "version": "0.7.23",
  "catchLinks": true
}